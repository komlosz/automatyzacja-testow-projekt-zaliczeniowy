package features;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import steps.LoginSteps;
import steps.RegisterSteps;

@RunWith(SerenityRunner.class)
public class Login {

		@Managed()
		WebDriver driver;

		@Steps
		RegisterSteps registerSteps;

		@Steps
		LoginSteps loginSteps;

		String email = RandomStringUtils.randomAlphabetic(6) + "@selenium.com";
		String password = "Test1234.!";

		@Before
		public void setupUser() {
			registerSteps.open();
			registerSteps.register(email, password, password);
			registerSteps.logout();
		}

		@Test
		public void userShouldLoginToApplication() {
			loginSteps.open();
			loginSteps.login(email, password);
			assertThat(driver.getTitle()).contains("Dashboard - Statistical Process Control");
		}
}
