package steps;

import net.thucydides.core.annotations.Step;
import pages.LoginPage;

public class LoginSteps {

	LoginPage loginPage;

	@Step
	public void open() {
		loginPage.open();
	}

	@Step
	public void login(String email, String password) {
		loginPage.setCredentials(email, password);
		loginPage.clickLogin();
	}
}
