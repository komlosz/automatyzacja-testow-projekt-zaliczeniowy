package pages;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:4444/Account/Logout")
public class LogoutPage extends PageObject {
}
