package pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:4444/Account/Login")
public class LoginPage extends PageObject {

	@FindBy(xpath = "//input[@id='Email']")
	WebElement emailInput;

	@FindBy(xpath = "//*[@id='Password']")
	WebElement passwordInput;

	@FindBy(xpath = "//button[contains(text(), 'Log in')]")
	WebElement loginButton;

	public void setCredentials(String email, String password) {
		emailInput.sendKeys(email);
		passwordInput.sendKeys(password);
	}

	public void clickLogin() {
		loginButton.click();
	}
}
