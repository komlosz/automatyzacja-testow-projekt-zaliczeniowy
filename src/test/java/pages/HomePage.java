package pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:4444")
public class HomePage extends PageObject {

	@FindBy(xpath = "//input[@id='Email']")
	WebElement emailInput;

	@FindBy(xpath = "//*[@id='Password']")
	WebElement passwordInput;

	@FindBy(xpath = "//button[contains(text(), 'Log in')]")
	WebElement loginButton;

	public void setCredentials(String email, String password) {
		emailInput.sendKeys(email);
		passwordInput.sendKeys(password);
	}

	public void clickLogin() {
		loginButton.click();
	}

	public void openHomeFromMenu() {
		find("//a[@class='menu-home']").click();
	}

	public void openWorkspaceFromMenu() {
		find("//a[@class='menu-workspace']").click();
	}

	public void openDashboard(){
		find("//a[text()='Dashboard']").click();
	}

	public void openProcesses(){
		find("//a[text()='Processes']").click();
	}

	public void openCharacteristics(){
		find("//a[text()='Characteristics']").click();
	}

	public boolean isProcessVisible(String value){
		return find(String.format("//*[@class='x_title']//h2[text()='%s']", value)).isVisible();
	}
}
