package steps;

import net.thucydides.core.annotations.Step;
import pages.LogoutPage;
import pages.RegisterPage;

public class RegisterSteps {

	RegisterPage registerPage;

	LogoutPage logoutPage;

	@Step
	public void open() {
		registerPage.open();
	}

	@Step
	public void logout(){
		logoutPage.open();
	}

	@Step
	public void register(String email, String password, String confirmPassword) {
		registerPage.setCredentials(email, password, confirmPassword);
		registerPage.clickRegister();
	}

	@Step
	public boolean passwordValidation(){
		return registerPage.passwordMatchValidation();
	}

}