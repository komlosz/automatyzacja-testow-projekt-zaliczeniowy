package steps;

import net.thucydides.core.annotations.Step;
import pages.ProcessCreatorPage;

public class ProcessCreationSteps {

	ProcessCreatorPage processCreatorPage;

	@Step
	public void open() {
		processCreatorPage.open();
	}

	@Step
	public void createNewProcess(String name, String desc, String notes) {
		processCreatorPage.setName(name);
		processCreatorPage.setDescription(desc);
		processCreatorPage.setNotes(notes);
		processCreatorPage.clickCreate();
	}

	@Step
	public boolean processIsVisibleInTheTable(String name, String desc, String notes) {
		if (processCreatorPage.processIsVisibleInTheTable(name) &&
				processCreatorPage.processIsVisibleInTheTable(desc) &&
				processCreatorPage.processIsVisibleInTheTable(notes)) {
			return true;
		} else {
			return false;
		}
	}
}
