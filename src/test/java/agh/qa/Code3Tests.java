package agh.qa;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Code3Tests {
    private Code3 _code3;

    @BeforeMethod
    public void SetUp(){
        _code3 = new Code3();
    }

    @DataProvider
    public Object[][] testDataProvider() {
        return new Object[][]{
                {0, -3, 0},
                {-1, -4, -7},
                {1, 1, 0},
        };
    }

    @Test(dataProvider = "testDataProvider")
    public void TestAll(int a, int b, int expected){
        int result = _code3.calculate(a,b);

        Assert.assertEquals(result, expected, "Verify result " + a + ", " + b);
    }
}
