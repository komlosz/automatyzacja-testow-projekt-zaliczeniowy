package features;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import steps.RegisterSteps;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SerenityRunner.class)
public class Register {

	@Managed()
	WebDriver driver;

	@Steps
	RegisterSteps registerSteps;

	@Test
	public void userShouldRegister() {
		String email = RandomStringUtils.randomAlphabetic(6) + "@selenium.com";
		String password = "Test1234.!";
		registerSteps.open();
		registerSteps.register(email, password, password);
		assertThat(driver.getTitle()).contains("Dashboard - Statistical Process Control");
	}

	@Test
	public void userShouldSeeCorrectPasswordValidation() {
		String email = RandomStringUtils.randomAlphabetic(6) + "@selenium.com";
		String password = "Test1234.!-1";
		String confirmPassword = "Test1234.!-2";
		registerSteps.open();
		registerSteps.register(email, password, confirmPassword);
		assertThat(registerSteps.passwordValidation()).isTrue();
	}
}