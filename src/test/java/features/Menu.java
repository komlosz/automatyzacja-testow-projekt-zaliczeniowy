package features;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import steps.HomeSteps;
import steps.RegisterSteps;

@RunWith(SerenityRunner.class)
public class Menu {

	@Managed()
	WebDriver driver;

	@Steps
	RegisterSteps registerSteps;

	@Steps
	HomeSteps homeSteps;

	String email = RandomStringUtils.randomAlphabetic(6) + "@selenium.com";
	String password = "Test1234.!";

	@Before
	public void setupUser() {
		registerSteps.open();
		registerSteps.register(email, password, password);
	}

	@Test
	public void userShouldNavigateUsingMenu() {
		homeSteps.openDashboardPage();
		assertThat(driver.getCurrentUrl()).contains("http://localhost:4444");

		homeSteps.openWorkspacePage();
		homeSteps.openProcessesPage();
		assertThat(driver.getCurrentUrl()).contains("http://localhost:4444/Projects");

		homeSteps.openCharacteristicsPage();
		assertThat(driver.getCurrentUrl()).contains("http://localhost:4444/Characteristics");
	}
}