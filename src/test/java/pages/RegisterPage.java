package pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:4444/Account/Register")
public class RegisterPage extends PageObject {

	@FindBy(xpath = "//input[@id='Email']")
	WebElement emailInput;

	@FindBy(xpath = "//*[@id='Password']")
	WebElement passwordInput;

	@FindBy(xpath = "//*[@id='ConfirmPassword']")
	WebElement passwordConfirmInput;

	@FindBy(xpath = "//button[contains(text(), 'Register')]")
	WebElement loginButton;

	public void setCredentials(String email, String password, String confirmPassword) {
		emailInput.sendKeys(email);
		passwordInput.sendKeys(password);
		passwordConfirmInput.sendKeys(confirmPassword);
	}

	public void clickRegister() {
		loginButton.click();
	}

	public boolean passwordMatchValidation(){
		return find("//*[contains(text(), 'The password and confirmation password do not match')]").isVisible();
	}
}
