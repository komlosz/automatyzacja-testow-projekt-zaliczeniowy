package features;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import steps.HomeSteps;
import steps.ProcessCreationSteps;
import steps.RegisterSteps;

@RunWith(SerenityRunner.class)
public class Process {

	@Managed()
	WebDriver driver;

	@Steps
	RegisterSteps registerSteps;

	@Steps
	HomeSteps homeSteps;

	@Steps
	ProcessCreationSteps processCreationSteps;

	String email = RandomStringUtils.randomAlphabetic(6) + "@selenium.com";
	String password = "Test1234.!";

	@Before
	public void setupUser() {
		registerSteps.open();
		registerSteps.register(email, password, password);
	}

	@Test
	public void userShouldSeeProcessInTheTable() {
		String processName = RandomStringUtils.randomAlphabetic(5);
		String processDesc = RandomStringUtils.randomAlphabetic(5);
		String processNotes = RandomStringUtils.randomAlphabetic(5);

		processCreationSteps.open();
		processCreationSteps.createNewProcess(processName, processDesc, processNotes);
		assertThat(processCreationSteps.processIsVisibleInTheTable(processName, processDesc, processNotes)).isTrue();
	}

	@Test
	public void userShouldSeeProcessInTheDashboard() {
		String processName = RandomStringUtils.randomAlphabetic(5);
		String processDesc = RandomStringUtils.randomAlphabetic(5);
		String processNotes = RandomStringUtils.randomAlphabetic(5);

		processCreationSteps.open();
		processCreationSteps.createNewProcess(processName, processDesc, processNotes);
		homeSteps.open();
		assertThat(homeSteps.isProcessVisible(processName)).isTrue();
	}
}
