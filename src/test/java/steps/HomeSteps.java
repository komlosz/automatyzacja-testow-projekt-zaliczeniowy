package steps;

import net.thucydides.core.annotations.Step;
import pages.HomePage;

public class HomeSteps {

	HomePage homePage;

	@Step
	public void open(){
		homePage.open();
	}

	@Step
	public void openHomePage() {
		homePage.openHomeFromMenu();
	}

	@Step
	public void openWorkspacePage() {
		homePage.openWorkspaceFromMenu();
	}

	@Step
	public void openDashboardPage() {
		homePage.openDashboard();
	}

	@Step
	public void openProcessesPage() {
		homePage.openProcesses();
	}

	@Step
	public void openCharacteristicsPage() {
		homePage.openCharacteristics();
	}

	@Step
	public boolean isProcessVisible(String name){
		return homePage.isProcessVisible(name);
	}
}
