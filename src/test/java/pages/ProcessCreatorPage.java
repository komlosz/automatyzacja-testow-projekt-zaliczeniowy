package pages;

import org.openqa.selenium.WebElement;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://localhost:4444/Projects/Create")
public class ProcessCreatorPage extends PageObject {

	@FindBy(xpath = "//*[@id='Name']")
	WebElement nameInput;

	@FindBy(xpath = "//*[@id='Description']")
	WebElement descriptionInput;

	@FindBy(xpath = "//*[@id='Notes']")
	WebElement notesInput;

	@FindBy(xpath = "//*[@value='Create']")
	WebElement createButton;

	public void setName(String name) {
		nameInput.sendKeys(name);
	}

	public void setDescription(String description) {
		descriptionInput.sendKeys(description);
	}

	public void setNotes(String notes) {
		notesInput.sendKeys(notes);
	}

	public void clickCreate() {
		createButton.click();
	}

	public boolean processIsVisibleInTheTable(String value){
		return find(String.format("//*[contains(text(),'%s')]", value)).isVisible();
	}
}
